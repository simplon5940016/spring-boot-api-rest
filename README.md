# Spring Boot - API REST

## Configuration


Configurez le fichier `src/main/resources/application.properties` avec les informations de connexion à votre base de données PostgreSQL :

   ```
   spring.datasource.url=jdbc:postgresql://localhost:5432/jpalibrary
   spring.datasource.username=votre_utilisateur
   spring.datasource.password=votre_mot_de_passe
   spring.jpa.hibernate.ddl-auto=update
   ```

## Lancer l'application

Exécutez l'application en utilisant la commande `mvn spring-boot:run`. L'application démarrera sur le port `8080` par défaut.

## Initialiser avec des livres

Avec POSTMAN

### Ajouter deux livres

1. Première fois :

   ```
   POST localhost:8080/books
   ```

   Avec le corps (body) suivant :

   ```json
   {
     "title": "Livre 1",
     "description": "Description",
     "available": true
   }
   ```

2. Deuxième fois :

   ```
   POST localhost:8080/books
   ```

   Avec le corps (body) suivant :

   ```json
   {
     "title": "Livre 2",
     "available": false
   }
   ```

### Vérifier si les livres ont été ajoutés

1. Récupérer tous les livres :

   ```
   GET localhost:8080/books
   ```

2. Récupérer un livre spécifique :

   ```
   GET localhost:8080/books/1
   ```

### Mettre à jour un livre

1. Mettre à jour le premier livre :

   ```
   PUT localhost:8080/books/1
   ```

   Avec le corps (body) suivant :

   ```json
   {
     "title": "Mise à jour Livre 1",
     "description": "Description du livre 1 mise à jour",
     "available": false
   }
   ```

2. Vérifier si le livre a été mis à jour :

   ```
   GET localhost:8080/books/1
   ```

### Supprimer un livre

1. Supprimer le premier livre :

   ```
   DELETE localhost:8080/books/1
   ```

2. Vérifier si le livre a été supprimé :

   ```
   GET localhost:8080/books
   ```

### Rechercher par titre un livre 

1. Créer un livre avec un titre facile 

   ```
   POST localhost:8080/books
   ```

   Avec le corps (body) suivant :

   ```json
   {
     "title": "Li",
     "description": "Livre",
     "available": false
   }
   ```

2. Rechercher par titre 

    ```json
    GET localhost:8080/books/title/Li
    ```

